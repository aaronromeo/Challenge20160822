require 'active_support'
require 'pry'

module DailyProgrammer
  class Challenge20160822 < Object
    attr_accessor :fingers_hash
    def initialize(fingers_hash)
      self.fingers_hash = fingers_hash
    end

    def position_of_highest_1(hand)
      hand[1..4].index('1').nil? ? -1 : 3 - hand[1..4].reverse.index('1')
    end

    def position_of_highest_0(hand)
      hand[1..4].index('0').nil? ? -1 : hand[1..4].index('0')
    end

    def value
      left_hand = self.fingers_hash[0..4].reverse
      right_hand = self.fingers_hash[5..9]

      position_of_highest_1 = {}
      position_of_highest_0 = {}

      position_of_highest_1[:lh] = position_of_highest_1 left_hand
      position_of_highest_0[:lh] = position_of_highest_0 left_hand

      position_of_highest_1[:rh] = position_of_highest_1 right_hand
      position_of_highest_0[:rh] = position_of_highest_0 right_hand

      if (position_of_highest_0[:lh] != -1 && position_of_highest_1[:lh] > position_of_highest_0[:lh]) || (position_of_highest_0[:rh] != -1 && position_of_highest_1[:rh] > position_of_highest_0[:rh])
        raise "invalid"
      end

      sum = ((position_of_highest_1[:lh] + 1) * 10) + (left_hand[0].to_i * 50) + (position_of_highest_1[:rh] + 1) + (right_hand[0].to_i * 5)
      sum
    end
  end
end

if __FILE__ == $0
  puts ARGV
  challenge = DailyProgrammer::Challenge20160822.new(ARGV[0].to_s)
  puts challenge.value
end
