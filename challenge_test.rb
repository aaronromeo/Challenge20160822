require 'minitest/autorun'
require_relative './challenge.rb'

class TestChallenge < MiniTest::Unit::TestCase
  def test_a_valid_case
    challenge = DailyProgrammer::Challenge20160822.new('0000110000')
    assert_equal challenge.value, 55
  end

  def test_a_valid_0_case
    challenge = DailyProgrammer::Challenge20160822.new('0000000000')
    assert_equal challenge.value, 0
  end

  def test_a_valid_1_case
    challenge = DailyProgrammer::Challenge20160822.new('1111111111')
    assert_equal challenge.value, 99
  end

  def test_an_invalid_case
    challenge = DailyProgrammer::Challenge20160822.new('1010010000')
    assert_raises RuntimeError do
      challenge.value
    end
  end
end
